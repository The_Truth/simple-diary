from django.conf import settings

from django.shortcuts import render
from django.http import HttpResponseRedirect
import csv
from pymongo import MongoClient
from django.core.files.storage import FileSystemStorage

from .forms import ConfigForm
from .forms import IdFormToAddDisc
from .forms import IdFormToShowDisc

from .forms import IdFormToDeleteDisc

from .forms import SubjectForm

from .forms import IdFormToModify
from .forms import TaskFormPZ
from .forms import MainTitleFormPZ
from .forms import TitlesFormPZ
from .forms import InfoFormPZ
from .forms import DatesFormPZ
from .forms import FormulaFormPZ

from .forms import TaskFormLR
from .forms import MainTitleFormLR
from .forms import TitlesFormLR
from .forms import InfoFormLR
from .forms import DatesFormLR
from .forms import FormulaFormLR

from .forms import TaskFormDDZ
from .forms import MainTitleFormDDZ
from .forms import TitlesFormDDZ
from .forms import InfoFormDDZ
from .forms import DatesFormDDZ
from .forms import FormulaFormDDZ

from .forms import ReaderForm

from .forms import TeacherForm
from .forms import DateForm

from .forms import TeacherFormE
from .forms import DateFormE

from django.views.generic import View

# Create your views here.

def add_disc(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session add_disc: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        doc_id_list = list()

        for post in posts.find():
            if 'description' not in list(post.keys()):
                doc_id_list.append(post["_id"])

        # if this is a POST request we need to process the form data
        if request.method == 'POST':
            print("echo 1")
            # create a form instance and populate it with data from the request:
            # form_config = ConfigForm(request.POST)
            form_id = IdFormToAddDisc(request.POST)
            # if form.is_valid():
                # data = form.cleaned_data.get("file_path")
            # check whether it's valid:
            if form_id.is_valid():

                # получаем загруженный файл
                # file = request.FILES['myfile1']
                name = request.FILES.get('myfile1', False)
                print(name)

                if name == False:
                    return render(request, 'discipline/add_disc.html',
                                  {'disc_msg_2': "Не выбран конфигурационный файл!",
                                   'doc_id_list': doc_id_list,
                                   # 'form_config': form_config,
                                   'form_id': form_id
                                   })
                else:
                    name = request.FILES['myfile1'].name

                    fs = FileSystemStorage()

                    fs.save(name, request.FILES['myfile1'])

                    with open(name) as csv_file:
                        csv_reader = csv.reader(csv_file, delimiter=';')
                        ed = list(csv_reader)

                    print(ed)

                    csv_file.close()
                    fs.delete(name)

                    # process the data in form.cleaned_data as required
                    # ...
                    # redirect to a new URL:
                    # data = form_config.cleaned_data.get("file_path")

                    # data_2 = request.POST.get("data")
                    # print(data_2)

                    # print("1.\n", configpath[0])

                    # work with csv file
                    # exampleFile = open("/home/user/Desktop/Diary/configs/timp.csv", encoding='UTF-8')
                    # exampleFile = open(data, encoding='UTF-8')

                    # exampleReader = csv.reader(exampleFile, delimiter=';')

                    # ed = list(exampleReader)

                    # print(ed)

                    # print("list size:", len(ed))

                    # for i in range(len(ed)):
                    # print(ed[i])

                    # exampleFile.close()

                    # print("\n\nSome other data:\n", ed[1][2], "\n\nTests:")

                    eo_dict = dict()
                    task_dict = dict()
                    global_task_dict = dict()

                    title_string = 'title_'
                    title_string_dict = ''
                    mark_string = 'mark_'
                    mark_string_dict = ''

                    # С помощью флагов смотрим  вхоождения на какие строки конфиг. файла были совершены
                    flag_1str = 0
                    flag_2str = 0
                    flag_3str = 0

                    flag_oe_1str = 0
                    flag_oe_2str = 0

                    j = 1

                    # на данном этапе обработка звездочки у названия глобального поля(такого как PZ_N) не происходит
                    while (True):

                        if ed[j][0].upper() != 'OFFSET' and ed[j][0].upper() != 'EXAM':
                            d = dict.fromkeys(['main_title'])

                            if len(ed[j][1].split()) == 1:
                                d['main_title'] = ed[j][1]
                            else:
                                d['main_title'] = 'none'

                            flag_1str = 1

                            if len(ed[j]) >= 2:
                                # т.е. если название ПЗ, ЛР и тд не указано
                                if len(ed[j][1].split()) != 1:
                                    # проверка на то, содержит ли строка поле pz/lr_info
                                    if len(ed[j][-1].split()) == 2:
                                        for i in range(1, len(ed[j])):
                                            title_string_dict = title_string + str(i)
                                            mark_string_dict = mark_string + str(i)

                                            if len(ed[j][i].split()) == 2:
                                                d[title_string_dict] = ed[j][i].split()[0]
                                                d[mark_string_dict] = ed[j][i].split()[1]
                                            if len(ed[j][
                                                       i].split()) == 1:  # логично, что преподаватель не станет задавать шкалу оценивания
                                                # без понимания для какого задания она предназначена, а значит:
                                                d[title_string_dict] = ed[j][i].split()[0]
                                                d[mark_string_dict] = 'none'

                                            title_string_dict = ''
                                            mark_string_dict = ''

                                        d['info'] = 'none'

                                    # проверка на то, содержит ли строка поле pz/lr_info
                                    if len(ed[j][-1].split()) == 1:
                                        for i in range(1, len(ed[j]) - 1):
                                            title_string_dict = title_string + str(i)
                                            mark_string_dict = mark_string + str(i)

                                            if len(ed[j][i].split()) == 2:
                                                d[title_string_dict] = ed[j][i].split()[0]
                                                d[mark_string_dict] = ed[j][i].split()[1]
                                            if len(ed[j][
                                                       i].split()) == 1:  # логично, что преподаватель не станет задавать шкалу оценивания
                                                # без понимания для какого задания она предназначена, а значит:
                                                d[title_string_dict] = ed[j][i].split()[0]
                                                d[mark_string_dict] = 'none'

                                            title_string_dict = ''
                                            mark_string_dict = ''

                                        d['info'] = ed[j][-1]

                                # т.е. если название ПЗ, ЛР и тд указано
                                if len(ed[j][1].split()) == 1:
                                    # проверка на то, содержит ли строка поле pz/lr_info
                                    if len(ed[j][-1].split()) == 2:
                                        for i in range(1, len(ed[j]) - 1):
                                            title_string_dict = title_string + str(i)
                                            mark_string_dict = mark_string + str(i)

                                            if len(ed[j][i + 1].split()) == 2:
                                                d[title_string_dict] = ed[j][i + 1].split()[0]
                                                d[mark_string_dict] = ed[j][i + 1].split()[1]
                                            if len(ed[j][
                                                       i + 1].split()) == 1:  # логично, что преподаватель не станет задавать шкалу оценивания
                                                # без понимания для какого задания она предназначена, а значит:
                                                d[title_string_dict] = ed[j][i + 1].split()[0]
                                                d[mark_string_dict] = 'none'

                                            title_string_dict = ''
                                            mark_string_dict = ''

                                        d['info'] = 'none'

                                    # проверка на то, содержит ли строка поле pz/lr_info
                                    if len(ed[j][-1].split()) == 1:
                                        for i in range(1, len(ed[j]) - 2):
                                            title_string_dict = title_string + str(i)
                                            mark_string_dict = mark_string + str(i)

                                            if len(ed[j][i + 1].split()) == 2:
                                                d[title_string_dict] = ed[j][i + 1].split()[0]
                                                d[mark_string_dict] = ed[j][i + 1].split()[1]
                                            if len(ed[j][
                                                       i + 1].split()) == 1:  # логично, что преподаватель не станет задавать шкалу оценивания
                                                # без понимания для какого задания она предназначена, а значит:
                                                d[title_string_dict] = ed[j][i + 1].split()[0]
                                                d[mark_string_dict] = 'none'

                                            title_string_dict = ''
                                            mark_string_dict = ''

                                        d['info'] = ed[j][-1]

                            if j < len(ed) - 1:
                                # print("\n", ed[j + 1][0], " <---> ", ed[j][0])
                                if ed[j + 1][0] == ed[j][0]:
                                    if 'student_mark' not in ed[j + 1][1]:
                                        if len(ed[j + 1]) == 3:
                                            d['good_date'] = ed[j + 1][1]
                                            d['final_date'] = ed[j + 1][2]
                                        if len(ed[j + 1]) == 2:
                                            d['good_date'] = 'none'
                                            d['final_date'] = ed[2][1]
                                        if len(ed[j + 1]) == 1:
                                            d['good_date'] = 'none'
                                            d['final_date'] = 'none'
                                    if 'student_mark' in ed[j + 1][1]:
                                        if len(ed[j + 1]) == 2:
                                            d['formula'] = ed[j + 1][1]
                                        if len(ed[j + 1]) == 1:
                                            d['formula'] = 'none'
                                    flag_2str = 1

                            if j < len(ed) - 2:
                                if ed[j + 2][0] == ed[j][0]:
                                    if len(ed[j + 2]) == 2:
                                        d['formula'] = ed[j + 2][1]
                                    if len(ed[j + 2]) == 1:
                                        d['formula'] = 'none'
                                    flag_3str = 1


                            # print("d: ", d)
                            if 'formula' not in list(d.keys()):
                                d['formula'] = 'none'
                            # добавляем структуру задания в общий словарь ПЗ, ЛР и тд
                            task_dict[ed[j][0]] = d

                            if flag_1str + flag_2str + flag_3str == 0:
                                print("\nEmpty csv config. file!")
                            if flag_1str + flag_2str + flag_3str == 1:
                                print("\nПерешли на след. занятие!")
                            if flag_1str + flag_2str + flag_3str == 2:
                                j += 1
                                print("\nДобавили +1 к итерации цикла, тк не все строки в занятии заполнены")
                            if flag_1str + flag_2str + flag_3str == 3:
                                j += 2
                                print("\nДобавили +2 к итерации цикла, тк не все строки в занятии заполнены")

                            flag_1str = 0
                            flag_2str = 0
                            flag_3str = 0

                            j += 1

                            # print("TASK  DICT: ", task_dict)
                            # Add task's ID - need to make delete and change operations
                            # task_dict_keys = list(task_dict.keys())
                            # for it in task_dict_keys:
                                # task = task_dict[it]
                                # task['task_id'] = it

                        if j >= len(ed):
                            break

                        if ed[j][0].upper() == 'OFFSET':
                            # print('OFFSET!')
                            if len(ed[j]) == 2:
                                if ed[j][1].upper() != ed[j][1]:
                                    eo_dict['teacher_fio'] = ed[j][1]
                                if ed[j][1].upper() == ed[j][1]:
                                    eo_dict['date'] = ed[j][1]

                                flag_oe_1str = 1

                            if j + 1 < len(ed):
                                if ed[j + 1][0] == ed[j][0]:
                                    if len(ed[j + 1]) == 2:
                                        if ed[j + 1][1].upper() != ed[j + 1][1]:
                                            eo_dict['teacher_fio'] = ed[j + 1][1]
                                        if ed[j + 1][1].upper() == ed[j + 1][1]:
                                            eo_dict['date'] = ed[j + 1][1]

                                flag_oe_2str = 1

                            task_dict[ed[j][0]] = eo_dict

                        if flag_oe_1str + flag_oe_2str == 0:
                            print('\nOffset does not exist')
                        if flag_oe_1str + flag_oe_2str == 1:
                            j += 1
                        if flag_oe_1str + flag_oe_2str == 2:
                            j += 2

                        flag_oe_1str = 0
                        flag_oe_2str = 0

                        if j >= len(ed):
                            break

                        if ed[j][0].upper() == 'EXAM':
                            # print('EXAM!')
                            if len(ed[j]) == 2:
                                if ed[j][1].upper() != ed[j][1]:
                                    eo_dict['teacher_fio'] = ed[j][1]
                                if ed[j][1].upper() == ed[j][1]:
                                    eo_dict['date'] = ed[j][1]

                                flag_oe_1str = 1

                            if j + 1 < len(ed):
                                if ed[j + 1][0] == ed[j][0]:
                                    if len(ed[j + 1]) == 2:
                                        if ed[j + 1][1].upper() != ed[j + 1][1]:
                                            eo_dict['teacher_fio'] = ed[j + 1][1]
                                        if ed[j + 1][1].upper() == ed[j + 1][1]:
                                            eo_dict['date'] = ed[j + 1][1]

                                flag_oe_2str = 1

                            task_dict[ed[j][0]] = eo_dict

                        if flag_oe_1str + flag_oe_2str == 0:
                            print('\nExam does not exist')
                        if flag_oe_1str + flag_oe_2str == 1:
                            j += 1
                        if flag_oe_1str + flag_oe_2str == 2:
                            j += 2

                        flag_oe_1str = 0
                        flag_oe_2str = 0

                        if j >= len(ed):
                            break

                    global_task_dict[ed[0][0]] = task_dict

                    post = global_task_dict
                    # filepath = input('New post path: ')  # вводим путь конфиг. файла
                    # filepath = "/home/user/Desktop/test.csv"  # используем конкретный путь для простоты проверки
                    # pz_counter - for student document's automatic creation
                    pz_counter = 0
                    # lr_counter - for student document's automatic creation
                    lr_counter = 0
                    # ddz_counter - for student document's automatic creation
                    ddz_counter = False
                    # offset_counter - for student document's automatic creation
                    offset_counter = False
                    # exam_counter - for student document's automatic creation
                    exam_counter = False
                    # list with numbers of PZ that exist in disc_documnet
                    pz_num = list()
                    # list with numbers of LR that exist in disc_documnet
                    lr_num = list()

                    # post = dict_maker(configpath)

                    post_key = list(post.keys())[0]
                    # print(post_key)
                    title_list = list(post[post_key])
                    # print(title_list)
                    for tmp in title_list:
                        if tmp.upper().startswith('PZ'):
                            pz_num.append(int(tmp[3:len(tmp)]))
                            pz_counter += 1
                        if tmp.upper().startswith('LR'):
                            lr_num.append(int(tmp[3:len(tmp)]))
                            lr_counter += 1
                        if tmp.upper().startswith('DDZ'):
                            ddz_counter = True
                        if tmp.upper().startswith('OFFSET'):
                            offset_counter = True
                        if tmp.upper().startswith('EXAM'):
                            exam_counter = True

                    # index = input('Введите индекс дисциплины: ')
                    # index = '1'  # для простоты проверки - возьмем конкретный сразу
                    post['_id'] = form_id.cleaned_data.get("new_id")
                    post['pz_counter'] = pz_counter
                    post['lr_counter'] = lr_counter
                    post['ddz_counter'] = ddz_counter
                    post['offset_counter'] = offset_counter
                    post['exam_counter'] = exam_counter
                    post['pz_numbers'] = pz_num
                    post['lr_numbers'] = lr_num
                    post_id = posts.save(post)
                    # print(post_id)

                    if post_id != None:
                        result = True
                    else:
                        result = False

                    return render(request, 'discipline/add_disc.html', {'disc_msg': "Дисциплина была успешно добавлена!",
                                                                        'doc_id_list': doc_id_list,
                                                                        # 'form_config': form_config,
                                                                        'form_id': form_id
                                                                        })
                    # return HttpResponseRedirect('get_disc')

        # if a GET (or any other method) we'll create a blank form
        else:
            form_config = ConfigForm()
            form_id = IdFormToAddDisc()

        return render(request, 'discipline/add_disc.html', {# 'form_config': form_config,
                                                            'form_id': form_id,
                                                            'doc_id_list': doc_id_list})

    else:
        return render(request, 'discipline/support.html')

    # return render(request, 'discipline/get_disc.html')


def get_disc(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    global id

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session get_disc: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        id_list = list()
        j = 1

        # post = posts.find_one({'_id': 'timp_v1'})

        for post in posts.find():
            if 'description' not in list(post.keys()):
                id_list.append(post["_id"])

        # form_id = IdFormToShowDisc(request.POST)

        if request.method == 'POST':
            # if form_id.is_valid():
            # id = form_id.cleaned_data.get("id_to_show")
            id = request.POST.get("Option_select")
            print(id)
            if id in id_list:
                # return render(request, 'discipline/disc_table.html')
                return HttpResponseRedirect('get_disc/disc_table')
            else:
                return render(request, 'discipline/get_disc.html', {'msg_error': 'Не найден ни один документ с таким ID. Попробуйте снова.',
                                                                    'id_list': id_list})
        else:
            print("NOT POST METHOD")
            # form_id = IdFormToShowDisc()

            post = {
                'id_list': id_list,
               # 'id_to_show': form_id,
            }

            return render(request, 'discipline/get_disc.html', post)
    else:
        return render(request, 'discipline/support.html')


def disc_table(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session disc_table: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        # disc_dict = dict()
        disc_dict_list = list()
        task_keys_list = list()

        # data_list = list()
        # global_data_list = list()

        keys_list = list()
        data_list = list()
        keys_and_data_dict = dict()
        keys_and_data_list = list()

        # table_id = dict()

        server_dict = dict()
        big_task_dict = dict()
        task_dict = dict()

        pz_num = list()
        lr_num = list()

        j = 1

        if request.method == 'GET':

            disc_dict = posts.find_one(id)
            print(id)

            subject = list(disc_dict.keys())
            print(subject[1])

            subject_dict = disc_dict[subject[1]]
            subject_keys = list(disc_dict[subject[1]].keys())

            for it in subject_keys:
                disc_dict_list.append(subject_dict[it])

                task_items = subject_dict[it].items()
                task_items_list = list(task_items)

                #print('\n\n', task_items)

                keys_list.append('task')
                data_list.append(it)
                for d in task_items:
                    keys_list.append(d[0])
                    data_list.append(d[1])

                # keys_list.pop()
                # data_list.pop()

                tmp_1 = keys_list.copy()
                tmp_2 = data_list.copy()
                keys_list.clear()
                data_list.clear()

                keys_and_data_dict['keys'] = tmp_1
                keys_and_data_dict['data'] = tmp_2
                keys_and_data_dict['table_id'] = 'table_' + str(j)

                tmp = keys_and_data_dict.copy()
                keys_and_data_dict.clear()
                keys_and_data_list.append(tmp)

                j += 1

            # print(keys_and_data_list)
            j = 0

            # print(keys_and_data_list)


            post = {
                'id': id,
                'subject': subject[1],
                'subject_keys': subject_keys,
                'keys_and_data_list': keys_and_data_list
            }
            print("GET")

            return render(request, 'discipline/disc_table.html', post)

        if request.method == 'POST':
            print("POST")
            data = request.POST.get("data")
            # print(data)

            mongo_dict = posts.find_one(id)
            keys = list(mongo_dict.keys())
            subject_dict = mongo_dict[keys[1]]
            subject_keys = list(subject_dict.keys())

            big_ajax_list = data.split('@')
            taskname_counter = 0

            for it in big_ajax_list:
                # print(it)


                ajax_list = it.split(';')
                if ajax_list[0] != subject_keys[taskname_counter]:
                    print("Taskname has changed!")

                    for sub_student in posts.find():
                        if mongo_dict["_id"] in sub_student["_id"] and mongo_dict["_id"] != sub_student["_id"]:
                            print(sub_student["_id"])
                            sub_student_keys = list(sub_student.keys())
                            sub_student_inside = sub_student[sub_student_keys[2]]
                            tmp = sub_student_inside[subject_keys[taskname_counter]].copy()
                            sub_student_inside[ajax_list[0]] = tmp
                            del sub_student_inside[subject_keys[taskname_counter]]
                            posts.update({"_id": sub_student["_id"]}, sub_student)



                length = len(ajax_list) - 1
                for i in range(1, int(length/2)+1):
                    task_dict[ajax_list[i]] = ajax_list[int(length/2)+i]

                tmp = task_dict.copy()
                big_task_dict[ajax_list[0]] = tmp
                task_dict.clear()

                taskname_counter += 1

            # print(big_task_dict)
            server_dict[keys[1]] = big_task_dict
            # print(server_dict)

            server_dict['pz_counter'] = mongo_dict['pz_counter']
            server_dict['lr_counter'] = mongo_dict['lr_counter']
            server_dict['ddz_counter'] = mongo_dict['ddz_counter']
            server_dict['offset_counter'] = mongo_dict['offset_counter']
            server_dict['exam_counter'] = mongo_dict['exam_counter']

            # check and change pz/lr_numbers if table was modified
            server_dict_keys = list(server_dict.keys())
            # print(server_dict_keys)
            server_subject_dict = server_dict[server_dict_keys[0]]
            server_subject_dict_keys = list(server_subject_dict.keys())
            # print(server_subject_dict_keys)

            for tmp in server_subject_dict_keys:
                if tmp.upper().startswith('PZ'):
                    pz_num.append(int(tmp[3:len(tmp)]))
                if tmp.upper().startswith('LR'):
                    lr_num.append(int(tmp[3:len(tmp)]))


            server_dict['pz_numbers'] = pz_num
            server_dict['lr_numbers'] = lr_num

            posts.update({"_id": id}, server_dict)

            return render(request, 'discipline/disc_table.html', {'sth': data})

        print("ALL")
    else:
        return render(request, 'discipline/support.html')


def delete_disc(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session delete_disc: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        id_list = list()

        for post in posts.find():
            if 'description' not in list(post.keys()):
                id_list.append(post["_id"])

        # form_id = IdFormToDeleteDisc()

        if request.method == 'POST':
            # create a form instance and populate it with data from the request:
            form_id = IdFormToDeleteDisc(request.POST)

            disc_msg_1 = ""
            disc_msg_2 = ""

            data = request.POST.get("Option_select")
            # if form_id.is_valid():
                # data = form_id.cleaned_data.get("disc_id_to_delete")
            if data in id_list:
                print(data)
                posts.remove({"_id": data})
                for del_doc in posts.find():
                    if data in del_doc["_id"]:
                        if 'description' in list(del_doc.keys()):
                            posts.remove({"_id": del_doc["_id"]})

                disc_msg_1 = "Дисциплина была успешно удалена!"
            else:
                disc_msg_2 = "Не найден ни один документ с таким ID. Попробуйте снова."

            return render(request, 'discipline/delete_disc.html', {'disc_msg_1': disc_msg_1,
                                                                       'disc_msg_2': disc_msg_2,
                                                                      # 'form_id': form_id,
                                                                       'id_list': id_list})
                # return HttpResponseRedirect('get_disc')

                # if a GET (or any other method) we'll create a blank form
           # else:
                # form_id = IdFormToDeleteDisc()

        return render(request, 'discipline/delete_disc.html', {# 'form_id': form_id,
                                                               'id_list': id_list})
    else:
        return render(request, 'discipline/support.html')


def modify_disc(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session modify_disc: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        doc_id_list = list()
        disc_dict = dict()

        for post in posts.find():
            if 'description' not in list(post.keys()):
                doc_id_list.append(post["_id"])

        form_id_doc = IdFormToAddDisc()
        form_subject = SubjectForm()

        if request.method == 'POST':
            print("POST")
            # create a form instance and populate it with data from the request:
            form_id_doc = IdFormToAddDisc(request.POST)
            form_subject = SubjectForm(request.POST)

            disc_msg_1 = ""
            disc_msg_2 = ""

            if form_subject.is_valid() and form_id_doc.is_valid():
                subject_name = form_subject.cleaned_data.get("subject_name")
                id_doc = form_id_doc.cleaned_data.get("new_id")

                print("sub_name: ", subject_name, "\nid_doc: ", id_doc)

                if id_doc not in doc_id_list:
                    disc_dict['_id'] = id_doc
                    disc_dict[subject_name] = dict()

                    disc_dict['pz_counter'] = 0
                    disc_dict['lr_counter'] = 0
                    disc_dict['ddz_counter'] = False
                    disc_dict['offset_counter'] = False
                    disc_dict['exam_counter'] = False
                    disc_dict['pz_numbers'] = list()
                    disc_dict['lr_numbers'] = list()

                    posts.save(disc_dict)
                    disc_msg_1 = "Макет дисциплины был успешно создан!"

                else:
                    disc_msg_2 = "Документ с таким ID уже сущетсвует"

                return render(request, 'discipline/modify_disc.html', {'disc_msg_1': disc_msg_1,
                                                                       'disc_msg_2': disc_msg_2,
                                                                       'form_id_doc': form_id_doc,
                                                                       'form_subject': form_subject,
                                                                       'doc_id_list': doc_id_list})

        return render(request, 'discipline/modify_disc.html', {'form_id_doc': form_id_doc,
                                                               'form_subject': form_subject,
                                                                'doc_id_list': doc_id_list,
                                                                'disc_msg_1': "",
                                                                'disc_msg_2': ""})
    else:
        return render('discipline/support.html')


def modify_disc_tasks(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    task_num_list = list()

    for i in range(1, 41):
        task_num_list.append(i)

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session modify_disc_tasks: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        doc_id_list = list()

        task_dict = dict()
        title_list = list()

        pz_counter = 0
        lr_counter = 0
        ddz_counter = False
        offset_counter = False
        exam_counter = False

        disc_msg_1 = ""
        disc_msg_2 = ""

        for post in posts.find():
            if 'description' not in list(post.keys()):
                doc_id_list.append(post["_id"])

        # form_id_modify = IdFormToModify()
        # form_task = TaskForm()
        form_main_title_pz = MainTitleFormPZ()
        form_title_pz = TitlesFormPZ()
        form_info_pz = InfoFormPZ()
        form_dates_pz = DatesFormPZ()
        form_formula_pz = FormulaFormPZ()

        form_main_title_lr = MainTitleFormLR()
        form_title_lr = TitlesFormLR()
        form_info_lr = InfoFormLR()
        form_dates_lr = DatesFormLR()
        form_formula_lr = FormulaFormLR()

        form_main_title_ddz = MainTitleFormDDZ()
        form_title_ddz = TitlesFormDDZ()
        form_info_ddz = InfoFormDDZ()
        form_dates_ddz = DatesFormDDZ()
        form_formula_ddz = FormulaFormDDZ()

        form_teacher = TeacherForm()
        form_offset_date = DateForm()

        form_teacher_e = TeacherFormE()
        form_exam_date = DateFormE()

        if request.method == 'POST':
            print("POST")

            selected_task = request.POST.get("Task_select")
            print("Task: ", selected_task)
            id_modify = request.POST.get('Option_select')
            print("id_modify: ", id_modify)


            print(selected_task.upper())

            if 'PZ' in selected_task.upper():

                task_num = request.POST.get("Task_num_pz")
                print("task_num: ", task_num)

                # create a form instance and populate it with data from the request:
                # form_id_modify = IdFormToModify(request.POST)
                # form_task = TaskForm(request.POST)
                form_main_title_pz = MainTitleFormPZ(request.POST)
                form_title_pz = TitlesFormPZ(request.POST)
                form_info_pz = InfoFormPZ(request.POST)
                form_dates_pz = DatesFormPZ(request.POST)
                form_formula_pz = FormulaFormPZ(request.POST)

                disc_msg_1 = ""
                disc_msg_2 = ""

                l = 0

                if form_main_title_pz.is_valid() and form_title_pz.is_valid() \
                            and form_info_pz.is_valid() and form_dates_pz.is_valid() and form_formula_pz.is_valid():

                    print('VALID')

                    # id_modify = form_id_modify.cleaned_data.get("id_to_modify")
                    # task = form_task.cleaned_data.get("task_name")
                    task = str(selected_task) + "_" + str(task_num)
                    main_title = form_main_title_pz.cleaned_data.get("main_title")
                    titles = form_title_pz.cleaned_data.get("titles")
                    info = form_info_pz.cleaned_data.get("info")
                    good_date = form_dates_pz.cleaned_data.get("good_date")
                    final_date = form_dates_pz.cleaned_data.get("final_date")
                    formula = form_formula_pz.cleaned_data.get("formula")

                    # print("id_modify: ", id_modify)
                    print("task: ", task)
                    print("main_title: ", main_title)
                    print("titles: ", titles)
                    print("info: ", info)
                    print("good_date: ", good_date)
                    print("final_date: ", final_date)
                    print("formula: ", formula)

                    if id_modify in doc_id_list:

                        task_dict['main_title'] = main_title

                        if ';' in titles:
                            # print("Yes, it is!")
                            title_list = titles.split(';')

                        for i in range(1, len(title_list) + 1):
                            name_1 = 'title_'
                            name_2 = 'mark_'
                            name_1 += str(i)
                            name_2 += str(i)

                            l += 1

                            task_dict[name_1] = title_list[i - 1]
                            task_dict[name_2] = ''

                        task_dict['info'] = info
                        task_dict['good_date'] = good_date
                        task_dict['final_date'] = final_date
                        task_dict['formula'] = formula
                        # print(task_dict)

                        post = posts.find_one(id_modify)
                        print("post from mongo: ", post)
                        post_keys = list(post.keys())
                        # print(post_keys)
                        subject_dict = post[post_keys[1]]
                        # subject_dict.update(task_dict)
                        subject_dict[task] = task_dict

                        # print(subject_dict)

                        for it in list(subject_dict.keys()):

                            if it.upper().startswith('PZ'):
                                # print("I am in PZ")

                                pz_counter += 1

                                number = it.split('_')
                                print(number[1])
                                number_int = int(number[1])
                                if number_int not in post['pz_numbers']:
                                    post['pz_numbers'].append(number_int)

                            # if it.upper().startswith('LR'):

                                # lr_counter += 1

                                # number = it.split('_')
                                # number_int = int(number[1])
                                # if number_int not in post['lr_numbers']:
                                    # post['lr_numbers'].append(number_int)

                            # if it.upper().startswith('DDZ'):
                                # ddz_counter = True

                            # if it.upper().startswith('OFFSET'):
                                # offset_counter = True

                            # if it.upper().startswith('EXAM'):
                                # exam_counter = True

                        post['pz_counter'] = pz_counter
                        # post['lr_counter'] = lr_counter
                        # post['ddz_counter'] = ddz_counter
                        # post['offset_counter'] = offset_counter
                        # post['exam_counter'] = exam_counter
                        # subject_dict['pz_numbers'] = pz_num
                        # subject_dict['lr_numbers'] = lr_num

                        posts.update({"_id": id_modify}, post)
                        print(post)

                        disc_msg_1 = "Дисциплина была успешно обновлена!"

                        qpz_mark_list = list()

                        for i in range(l):
                            qpz_mark_list.append("")

                        qpz_dict = {'marks': qpz_mark_list,
                                    'stud_date': '', 'plag': '', 'summary': 'none'}

                        for ita in posts.find():
                            if id_modify in ita["_id"] and id_modify != ita["_id"]:
                                ita_keys = list(ita.keys())
                                sub_1 = ita[ita_keys[2]]
                                sub_1[task] = qpz_dict

                                print(ita)

                                posts.update({"_id": ita['_id']}, ita)

                    else:
                        disc_msg_2 = "Документа с таким ID не существует"


                    return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

            if 'LR' in selected_task.upper():

                task_num = request.POST.get("Task_num_lr")
                print("task_num: ", task_num)

                # create a form instance and populate it with data from the request:
                # form_id_modify = IdFormToModify(request.POST)
                # form_task = TaskForm(request.POST)
                form_main_title_lr = MainTitleFormLR(request.POST)
                form_title_lr = TitlesFormLR(request.POST)
                form_info_lr = InfoFormLR(request.POST)
                form_dates_lr = DatesFormLR(request.POST)
                form_formula_lr = FormulaFormLR(request.POST)

                disc_msg_1 = ""
                disc_msg_2 = ""

                l = 0

                if form_main_title_lr.is_valid() and form_title_lr.is_valid() \
                            and form_info_lr.is_valid() and form_dates_lr.is_valid() and form_formula_lr.is_valid():

                    print('VALID')

                    # id_modify = form_id_modify.cleaned_data.get("id_to_modify")
                    # task = form_task.cleaned_data.get("task_name")
                    task = str(selected_task) + "_" + str(task_num)
                    main_title = form_main_title_lr.cleaned_data.get("main_title_lr")
                    titles = form_title_lr.cleaned_data.get("titles_lr")
                    info = form_info_lr.cleaned_data.get("info_lr")
                    good_date = form_dates_lr.cleaned_data.get("good_date_lr")
                    final_date = form_dates_lr.cleaned_data.get("final_date_lr")
                    formula = form_formula_lr.cleaned_data.get("formula_lr")

                    # print("id_modify: ", id_modify)
                    print("task: ", task)
                    print("main_title: ", main_title)
                    print("titles: ", titles)
                    print("info: ", info)
                    print("good_date: ", good_date)
                    print("final_date: ", final_date)
                    print("formula: ", formula)

                    if id_modify in doc_id_list:

                        task_dict['main_title'] = main_title

                        if ';' in titles:
                            # print("Yes, it is!")
                            title_list = titles.split(';')

                        for i in range(1, len(title_list) + 1):
                            name_1 = 'title_'
                            name_2 = 'mark_'
                            name_1 += str(i)
                            name_2 += str(i)

                            l += 1

                            task_dict[name_1] = title_list[i - 1]
                            task_dict[name_2] = ''

                        task_dict['info'] = info
                        task_dict['good_date'] = good_date
                        task_dict['final_date'] = final_date
                        task_dict['formula'] = formula
                        # print(task_dict)

                        post = posts.find_one(id_modify)
                        print("post from mongo: ", post)
                        post_keys = list(post.keys())
                        # print(post_keys)
                        subject_dict = post[post_keys[1]]
                        # subject_dict.update(task_dict)
                        subject_dict[task] = task_dict

                        # print(subject_dict)

                        for it in list(subject_dict.keys()):

                            # if it.upper().startswith('PZ'):
                                # print("I am in PZ")

                                # pz_counter += 1

                                # number = it.split('_')
                                # print(number[1])
                                # number_int = int(number[1])
                                # if number_int not in post['pz_numbers']:
                                    # post['pz_numbers'].append(number_int)

                            if it.upper().startswith('LR'):

                                lr_counter += 1

                                number = it.split('_')
                                number_int = int(number[1])
                                if number_int not in post['lr_numbers']:
                                    post['lr_numbers'].append(number_int)

                            # if it.upper().startswith('DDZ'):
                                # ddz_counter = True

                            # if it.upper().startswith('OFFSET'):
                                # offset_counter = True

                            # if it.upper().startswith('EXAM'):
                                # exam_counter = True

                        # post['pz_counter'] = pz_counter
                        post['lr_counter'] = lr_counter
                        # post['ddz_counter'] = ddz_counter
                        # post['offset_counter'] = offset_counter
                        # post['exam_counter'] = exam_counter
                        # subject_dict['pz_numbers'] = pz_num
                        # subject_dict['lr_numbers'] = lr_num

                        posts.update({"_id": id_modify}, post)
                        print(post)

                        disc_msg_1 = "Дисциплина была успешно обновлена!"

                        qlr_mark_list = list()

                        for i in range(l):
                            qlr_mark_list.append("")

                        qlr_dict = {'marks': qlr_mark_list,
                                    'stud_date': '', 'plag': '', 'summary': 'none'}

                        for ita in posts.find():
                            if id_modify in ita["_id"] and id_modify != ita["_id"]:
                                ita_keys = list(ita.keys())
                                sub_1 = ita[ita_keys[2]]
                                sub_1[task] = qlr_dict

                                print(ita)

                                posts.update({"_id": ita['_id']}, ita)

                    else:
                        disc_msg_2 = "Документа с таким ID не существует"


                    return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })


            if 'DDZ' in selected_task.upper():

                form_main_title_ddz = MainTitleFormDDZ(request.POST)
                form_title_ddz = TitlesFormDDZ(request.POST)
                form_info_ddz = InfoFormDDZ(request.POST)
                form_dates_ddz = DatesFormDDZ(request.POST)
                form_formula_ddz = FormulaFormDDZ(request.POST)

                disc_msg_1 = ""
                disc_msg_2 = ""

                l = 0

                if form_main_title_ddz.is_valid() and form_title_ddz.is_valid() \
                        and form_info_ddz.is_valid() and form_dates_ddz.is_valid() and form_formula_ddz.is_valid():

                    # id_modify = form_id_modify.cleaned_data.get("id_to_modify")
                    # task = form_task.cleaned_data.get("task_name")
                    task = str(selected_task)
                    main_title = form_main_title_ddz.cleaned_data.get("main_title_ddz")
                    titles = form_title_ddz.cleaned_data.get("titles_ddz")
                    info = form_info_ddz.cleaned_data.get("info_ddz")
                    good_date = form_dates_ddz.cleaned_data.get("good_date_ddz")
                    final_date = form_dates_ddz.cleaned_data.get("final_date_ddz")
                    formula = form_formula_ddz.cleaned_data.get("formula_ddz")

                    # print("id_modify: ", id_modify)
                    print("task: ", task)
                    print("main_title: ", main_title)
                    print("titles: ", titles)
                    print("info: ", info)
                    print("good_date: ", good_date)
                    print("final_date: ", final_date)
                    print("formula: ", formula)

                    if id_modify in doc_id_list:

                        task_dict['main_title'] = main_title

                        if ';' in titles:
                            # print("Yes, it is!")
                            title_list = titles.split(';')

                        for i in range(1, len(title_list) + 1):
                            name_1 = 'title_'
                            name_2 = 'mark_'
                            name_1 += str(i)
                            name_2 += str(i)

                            l += 1

                            task_dict[name_1] = title_list[i - 1]
                            task_dict[name_2] = ''

                        task_dict['info'] = info
                        task_dict['good_date'] = good_date
                        task_dict['final_date'] = final_date
                        task_dict['formula'] = formula
                        # print(task_dict)

                        post = posts.find_one(id_modify)
                        print("post from mongo: ", post)
                        post_keys = list(post.keys())
                        # print(post_keys)
                        subject_dict = post[post_keys[1]]
                        # subject_dict.update(task_dict)
                        subject_dict[task] = task_dict

                        # print(subject_dict)

                        ddz_counter = True

                        post['ddz_counter'] = ddz_counter

                        # subject_dict['pz_numbers'] = pz_num
                        # subject_dict['lr_numbers'] = lr_num

                        posts.update({"_id": id_modify}, post)
                        print(post)

                        disc_msg_1 = "Дисциплина была успешно обновлена!"

                        ddz_mark_list = list()

                        for i in range(l):
                            ddz_mark_list.append("")

                        ddz_dict = {'marks': ddz_mark_list,
                                    'stud_date': '', 'plag': '', 'summary': 'none'}

                        for ita in posts.find():
                            if id_modify in ita["_id"] and id_modify != ita["_id"]:
                                ita_keys = list(ita.keys())
                                sub_1 = ita[ita_keys[2]]
                                sub_1['DDZ'] = ddz_dict

                                print(ita)

                                posts.update({"_id": ita['_id']}, ita)

                    else:
                        disc_msg_2 = "Документа с таким ID не существует"

                    return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

            l = 0

            if 'OFFSET' in selected_task.upper():
                print("OFFSET!")
                task = str(selected_task)
                form_teacher = TeacherForm(request.POST)
                form_offset_date = DateForm(request.POST)

                disc_msg_1 = ""
                disc_msg_2 = ""

                if form_teacher.is_valid() and form_offset_date.is_valid():

                    if id_modify in doc_id_list:

                        print(id_modify)
                        teacher = form_teacher.cleaned_data.get("teacher")
                        offset_date = form_offset_date.cleaned_data.get("date")
                        # print("teacher: ", teacher)

                        task_dict['teacher_fio'] = teacher
                        task_dict['date'] = offset_date

                        post = posts.find_one(id_modify)
                        print("post from mongo: ", post)
                        post_keys = list(post.keys())
                        # print(post_keys)
                        subject_dict = post[post_keys[1]]
                        # subject_dict.update(task_dict)
                        subject_dict[task] = task_dict

                        print(subject_dict)

                        offset_counter = True
                        post['offset_counter'] = offset_counter

                        posts.update({"_id": id_modify}, post)

                        disc_msg_1 = "Дисциплина была успешно обновлена!"

                        off_dict = {'offset_mark': 'none'}

                        for ita in posts.find():
                            if id_modify in ita["_id"] and id_modify != ita["_id"]:
                                ita_keys = list(ita.keys())
                                sub_1 = ita[ita_keys[2]]
                                sub_1['Offset'] = off_dict

                                print(ita)

                                posts.update({"_id": ita['_id']}, ita)

                        return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

                    else:

                        disc_msg_2 = "Документа с таким ID не существует"

                        return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

                else:
                    return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

            if 'EXAM' in selected_task.upper():
                print("EXAM!")
                task = str(selected_task)
                form_teacher_e = TeacherFormE(request.POST)
                form_exam_date = DateFormE(request.POST)

                disc_msg_1 = ""
                disc_msg_2 = ""

                if form_teacher_e.is_valid() and form_exam_date.is_valid():

                    if id_modify in doc_id_list:

                        print(id_modify)
                        teacher = form_teacher_e.cleaned_data.get("teacher_e")
                        exam_date = form_exam_date.cleaned_data.get("date_e")
                        # print("teacher: ", teacher)

                        task_dict['teacher_fio'] = teacher
                        task_dict['date'] = exam_date

                        post = posts.find_one(id_modify)
                        print("post from mongo: ", post)
                        post_keys = list(post.keys())
                        # print(post_keys)
                        subject_dict = post[post_keys[1]]
                        # subject_dict.update(task_dict)
                        subject_dict[task] = task_dict

                        print(subject_dict)

                        exam_counter = True
                        post['exam_counter'] = exam_counter

                        posts.update({"_id": id_modify}, post)

                        disc_msg_1 = "Дисциплина была успешно обновлена!"

                        exq_dict = {'exam_mark': 'none'}

                        for ita in posts.find():
                            if id_modify in ita["_id"] and id_modify != ita["_id"]:
                                ita_keys = list(ita.keys())
                                sub_1 = ita[ita_keys[2]]
                                sub_1['Exam'] = exq_dict

                                print(ita)

                                posts.update({"_id": ita['_id']}, ita)

                        return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

                    else:

                        disc_msg_2 = "Документа с таким ID не существует"

                        return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

                else:
                    return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

        return render(request, 'discipline/modify_disc_tasks.html', {'disc_msg_1': disc_msg_1,
                                                                                 'disc_msg_2': disc_msg_2,
                                                                                 # 'form_id_modify': form_id_modify,
                                                                                 # 'form_task': form_task,
                                                                                 'form_main_title_pz': form_main_title_pz,
                                                                                 'form_title_pz': form_title_pz,
                                                                                 'form_dates_pz': form_dates_pz,
                                                                                 'form_info_pz': form_info_pz,
                                                                                 'form_formula_pz': form_formula_pz,

                                                                                 'form_main_title_ddz': form_main_title_ddz,
                                                                                 'form_title_ddz': form_title_ddz,
                                                                                 'form_dates_ddz': form_dates_ddz,
                                                                                 'form_info_ddz': form_info_ddz,
                                                                                 'form_formula_ddz': form_formula_ddz,

                                                                                  'form_main_title_lr': form_main_title_lr,
                                                                                  'form_title_lr': form_title_lr,
                                                                                  'form_dates_lr': form_dates_lr,
                                                                                  'form_info_lr': form_info_lr,
                                                                                  'form_formula_lr': form_formula_lr,

                                                                                 'doc_id_list': doc_id_list,
                                                                                 'task_num_list': task_num_list,

                                                                                 'form_teacher': form_teacher,
                                                                                 'form_offset_date': form_offset_date,

                                                                                 'form_teacher_e': form_teacher_e,
                                                                                 'form_exam_date': form_exam_date,
                                                                                 })

    else:
        return render(request, 'discipline/support.html')


def test(request):
    print("TEST")
    return render(request, 'discipline/support.html')


