from django.urls import path
from . import views
from .views import *

urlpatterns = [
    path('add_disc', views.add_disc, name='add_disc'),
    path('get_disc', views.get_disc, name='get_disc'),
    path('delete_disc', views.delete_disc, name='delete_disc'),
    path('get_disc/disc_table', views.disc_table, name='disc_table'),
    path('get_disc/disc_table/test', views.test, name='test'),
    path('modify_disc', views.modify_disc, name='modify_disc'),
    path('modify_disc_tasks', views.modify_disc_tasks, name='modify_disc_tasks'),
    path('support', views.test, name='support')
]