from django import forms
from .models import Reader


class ConfigForm(forms.Form):
    file_path = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "/folder_1/folder_2/.../config.csv",
                                                                        'size': '34',
                                                                        'id': 'path_id'}), max_length=100)

class IdFormToAddDisc(forms.Form):
    new_id = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "ID нового документа дисциплины",
                                                                         'size': '34'}), max_length=100)


class IdFormToShowDisc(forms.Form):
    id_to_show = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Discipline's document ID to show",
                                                                         'size': '34'}), max_length=100)


class IdFormToDeleteDisc(forms.Form):
    disc_id_to_delete = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Discipline's document ID to delete",
                                                                         'size': '34'}), max_length=100)


class SubjectForm(forms.Form):
    subject_name = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Название",
                                                                         'size': '34'}), max_length=100)


class IdFormToModify(forms.Form):
    id_to_modify = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "ID документа дисциплины для обновления",
                                                                         'size': '34'}), max_length=100)


class TaskFormPZ(forms.Form):
    task_name = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Номер занятия",
                                                                         'size': '34'}), max_length=100)


class MainTitleFormPZ(forms.Form):
    main_title = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Тема занятия",
                                                                         'size': '34',
                                                                         'value': 'none'}), max_length=100)


class TitlesFormPZ(forms.Form):
    titles = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "задание_1; задание_2; ...",
                                                                     'size': '34',
                                                                     'value': 'none'}), max_length=100)


class InfoFormPZ(forms.Form):
    info = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Информация",
                                                                   'size': '34',
                                                                   'value': 'none'}), max_length=100)


class DatesFormPZ(forms.Form):
    good_date = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "01.01.2010",
                                                                         'size': '34',
                                                                         'value': 'Льготная дата'}), max_length=100)
    final_date = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "10.10.2010",
                                                                        'size': '34',
                                                                        'value': 'Крайняя дата'}), max_length=100)


class FormulaFormPZ(forms.Form):
    formula = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "1.5*student_mark_1 + 1*student_mark_2 + ...",
                                                                      'size': '73',
                                                                      'value': 'none'
                                                                      }), max_length=100)


class TaskFormLR(forms.Form):
    task_name_lr = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Номер работы",
                                                                         'size': '34'}), max_length=100)


class MainTitleFormLR(forms.Form):
    main_title_lr = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Тема работы",
                                                                         'size': '34',
                                                                         'value': 'none'}), max_length=100)


class TitlesFormLR(forms.Form):
    titles_lr = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "задание_1; задание_2; ...",
                                                                     'size': '34',
                                                                     'value': 'none'}), max_length=100)


class InfoFormLR(forms.Form):
    info_lr = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Информация",
                                                                   'size': '34',
                                                                   'value': 'none'}), max_length=100)


class DatesFormLR(forms.Form):
    good_date_lr = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "01.01.2010",
                                                                         'size': '34',
                                                                         'value': 'Льготная дата'}), max_length=100)
    final_date_lr = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "10.10.2010",
                                                                        'size': '34',
                                                                        'value': 'Крайняя дата'}), max_length=100)


class FormulaFormLR(forms.Form):
    formula_lr = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "1.5*student_mark_1 + 1*student_mark_2 + ...",
                                                                      'size': '73',
                                                                      'value': 'none'
                                                                      }), max_length=100)



class TaskFormDDZ(forms.Form):
    task_name_ddz = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Номер занятия",
                                                                         'size': '34'}), max_length=100)


class MainTitleFormDDZ(forms.Form):
    main_title_ddz = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Тема ДДЗ",
                                                                         'size': '34',
                                                                         'value': 'none'}), max_length=100)


class TitlesFormDDZ(forms.Form):
    titles_ddz = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "задание_1; задание_2; ...",
                                                                     'size': '34',
                                                                     'value': 'none'}), max_length=100)


class InfoFormDDZ(forms.Form):
    info_ddz = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Информация",
                                                                   'size': '34',
                                                                   'value': 'none'}), max_length=100)


class DatesFormDDZ(forms.Form):
    good_date_ddz = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "01.01.2010",
                                                                         'size': '34',
                                                                         'value': 'Льготная дата'}), max_length=100)
    final_date_ddz = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "10.10.2010",
                                                                        'size': '34',
                                                                        'value': 'Крайняя дата'}), max_length=100)


class FormulaFormDDZ(forms.Form):
    formula_ddz = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "1.5*student_mark_1 + 1*student_mark_2 + ...",
                                                                      'size': '73',
                                                                      'value': 'none'
                                                                      }), max_length=100)


class ReaderForm(forms.ModelForm):
    class Meta:
        model = Reader
        fields = ['file']


class TeacherForm(forms.Form):
    teacher = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "ФИО преподавателя",
                                                                   'size': '34',
                                                                   'value': 'none'}), max_length=100)

class DateForm(forms.Form):
    date = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Дата зачета",
                                                                   'size': '34',
                                                                   'value': 'none'}), max_length=100)


class TeacherFormE(forms.Form):
    teacher_e = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "ФИО преподавателя",
                                                                   'size': '34',
                                                                   'value': 'none'}), max_length=100)

class DateFormE(forms.Form):
    date_e = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Дата экзамена",
                                                                   'size': '34',
                                                                   'value': 'none'}), max_length=100)