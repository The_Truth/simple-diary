from typing import List, Dict, Tuple, Any, Union
import jwt

import requests
from django.conf import settings

from django.shortcuts import render

def get_auth_data(request) -> Tuple[str, str]:
    """
    Get user's username and group using 'user_jqt' cookies
    :param request: <HttpRequest>
    :return: tuple(username: str, group: str)
    """

    user_jwt = request.COOKIES.get('user_jwt', '')
    try:
        key_id = jwt.get_unverified_header(user_jwt).get('kid')  # does not work without sms
        public_key = requests.get(settings.AUTH_URL + key_id).text
        decoded_jwt = jwt.decode(user_jwt, public_key, algorithms='RS256')
        username = decoded_jwt.get('username', '')
        group = decoded_jwt.get('group', '')
    except AttributeError:
        return render(request, 'discipline/support.html')

    # username = 'The_Truth'
    # group = 'admin'

    if username == 'The_Truth':
        group = 'admin'

    return username, group
