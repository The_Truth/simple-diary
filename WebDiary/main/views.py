from django.shortcuts import render
from django.http import HttpResponseRedirect
from pymongo import MongoClient
from django.shortcuts import redirect

from . import utils
# from .models import UserModel

from django.contrib.auth.models import User
from django.contrib.auth import login, logout
# from jwt import DecodeError

from .decorators import unauthenticated_user
from django.utils.decorators import method_decorator

from django.views.generic import View

# Create your views here.


# @unauthenticated_user



def home(request):

    group_list = ['admin', 'student', 'teacher']

    try:
        username, group = utils.get_auth_data(request)
    except ValueError:
        return render(request, 'discipline/support.html')

    user = request.session.get('username')
    gr = request.session.get('group')

    print(user, gr)

    if not user or not gr:
        print('RIGHT')
        request.session['username'] = username
        request.session['group'] = group

    user = request.session.get('username')
    gr = request.session.get('group')

    print(user, gr)

    if gr in group_list:

        if gr == 'admin' or gr == 'teacher':
            return render(request, 'main/home.html')
        if gr == 'student':
            # return render(request, 'student/S_all_stud_table.html')
            return HttpResponseRedirect('student/student_all_stud_table')
    else:
        return render(request, 'discipline/support.html')