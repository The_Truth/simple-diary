from django.shortcuts import redirect, reverse


def unauthenticated_user(view_func):
    """
    Checked if user is authorized
    """
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return view_func(request, *args, **kwargs)
        else:
            return redirect(reverse('discipline:get_disc')) # need to change to other page
    return wrapper_func