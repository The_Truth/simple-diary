function help(){
  alert("Таблица обновлена!")
}


function stud_delete(){
  alert("Студент был успешно удален!")
}


function start(){
  $.ajax({
        url: URL,
        type: 'post',
        data: {'data': 'sth'},
        headers: {"X-CSRFToken": csrfToken},
        success: help()
    })
}


// table in disc_table.html
function json_send() {

    let table_id_string = Array.from(new Set(Array.from(document.all).map(i => i.id).filter(i => i != ""))).sort().join(';')
    let table_id_list = table_id_string.split(';')

    //alert(table_id_list.length)

    let table;
    let task_dict = [];
    let big_task_dict = [];


    for (var i = 0; i < table_id_list.length; i++)
    {
        table = document.getElementById(table_id_list[i]);

        for (var r = 0, n = table.rows.length; r < n; r++)
        {
            if (r == 0)
            {
                task_dict.push(table.rows[1].cells[0].innerHTML);
            }
            for (var c = 1, m = table.rows[r].cells.length; c < m; c++)
            {
                //alert(table.rows[r].cells[c].innerHTML);
                task_dict.push(table.rows[r].cells[c].innerHTML);
            }
        }
        str_inside = task_dict.join(';')
        big_task_dict.push(str_inside);
        task_dict = [];
    }


    str = big_task_dict.join('@');
    //alert(str);

    $.ajax({
        url: URL,
        type: 'post',
        //dataType: 'json',
        data: {'data': str},
        headers: {"X-CSRFToken": csrfToken},
        success: help()
    })
}


// table in all_stud_table.html
function all_student_send(table_id) {

    // let table_id_string = Array.from(new Set(Array.from(document.all).map(i => i.id).filter(i => i != ""))).sort().join(';')
    // let table_id_list = table_id_string.split(';')

    //alert(table_id_list.length)

    table = document.getElementById(table_id);
    let data = [];
    let some_data = [];

    for (var r = 1, n = table.rows.length; r < n; r++)
    {

        for (var c = 0, m = table.rows[r].cells.length; c < m; c++)
        {
            //alert(table.rows[r].cells[c].innerHTML);
            data.push(table.rows[r].cells[c].innerHTML);

        }
        data_str = data.join(';')
        some_data.push(data_str);
        data = [];
    }

    //alert(some_data);
    some_data_str = some_data.join('@');

    $.ajax({
        url: URL,
        type: 'post',
        //dataType: 'json',
        data: {'data': some_data_str},
        headers: {"X-CSRFToken": csrfToken},
        success: help()
    })

}


function all_stud_table_color()
{
    table = document.getElementById('all_student');


    for (var r = 1, n = table.rows.length; r < n; r++)
    {

        for (var c = 0, m = table.rows[r].cells.length; c < m; c++)
        {
            //alert(table.rows[r].cells[c].innerHTML);
            temp = table.rows[r].cells[c].innerHTML;
            //temp = temp.replace(/ +/g, ' ').trim();
            temp = temp.replace(/\s/g, '');

            if (temp.includes('+') && temp.length == 1)
            {
                //alert(temp);
                table.rows[r].cells[c].style.backgroundColor = 'green';
            }
            if (temp.includes('-') && temp.length == 1)
            {
                //alert(temp);
                table.rows[r].cells[c].style.backgroundColor = 'red';
            }
            if (temp.includes('*') && temp.length == 1)
            {
                //alert(temp);
                table.rows[r].cells[c].style.backgroundColor = 'yellow';
            }

        }
    }
}


function fullname_choice(){
    let selectedFullname = $('select option:selected' ).text();

    let fullname_index = selectedFullname.lastIndexOf("List");

    let fullname = selectedFullname.substr(fullname_index + 4);

    // alert(fullname.length);

    if (fullname.length > 0) {

        if (confirm("Вы уверены, что хотите удалить этот документ?") == true) {
            $.ajax({
            url: URL,
            type: 'post',
            //dataType: 'json',
            data: {'data': fullname},
            headers: {"X-CSRFToken": csrfToken},
            success: stud_delete()
            });
        }
    }
    else
    {
        alert("Выбран неверный ID студента для удаления")
    }

}


function convert_stud()
{
    $("#all_student").tableHTMLExport({
              type:"pdf",
              filename:"table.pdf",
          });

    //alert("convert");
}


function convert_disc()
{
    $("#table_1").tableHTMLExport({
              type:"pdf",
              filename:"table.pdf",
          });

    //alert("convert");
}


function get_path()
{
    path = document.getElementById('path_id').value;

    //$.ajax({
    //    url: URL,
    //    type: 'post',
    //    //dataType: 'json',
    //    data: {'data': path},
    //    headers: {"X-CSRFToken": csrfToken}
    //})
}

/*

       $.ajax({
        url: URL,
        type: 'post',
        data: {'data': 'sth'},
        headers: {"X-CSRFToken": csrfToken},
        success: help()
    })
}
*/