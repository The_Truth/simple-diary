from django import forms

# get_stud.html
class StudentFullnameForm(forms.Form):
    fullname = forms.CharField(label="",
                               widget=forms.TextInput(attrs={'placeholder': "Student's fullname",
                                                               'size': '36',
                                                               'value': 'null'}), max_length=100)


class StudentFioForm(forms.Form):
    fio = forms.CharField(label="",
                          widget=forms.TextInput(attrs={'placeholder': "Student's fio",
                                                        'size': '36',
                                                        'value': 'null'}), max_length=100)


class StudentFioRodForm(forms.Form):
    fio_rod = forms.CharField(label="",
                              widget=forms.TextInput(attrs={'placeholder': "Student's fio in rod.",
                                                            'size': '36',
                                                            'value': 'null'}), max_length=100)


class StudentRankForm(forms.Form):
    rank = forms.CharField(label="",
                           widget=forms.TextInput(attrs={'placeholder': "Student's rank",
                                                         'size': '36',
                                                         'value': 'null'}), max_length=100)



class StudentIdDocumentConnect(forms.Form):
    id_doc = forms.CharField(label="",
                           widget=forms.TextInput(attrs={'placeholder': "ID of Discipline's document to connect",
                                                         'size': '36',
                                                         'value': 'null'}), max_length=100)


class IdFormToShowStud(forms.Form):
    id_to_show = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': "Student's document ID to show",
                                                                         'size': '36'}), max_length=100)


# get_all_stud.html
class DiscDocID(forms.Form):
    disc_id = forms.CharField(label="",
                           widget=forms.TextInput(attrs={'placeholder': "ID of Discipline's document",
                                                         'size': '36',
                                                         'value': 'null'}), max_length=100)


class StudentYear(forms.Form):
    year = forms.CharField(label="",
                           widget=forms.TextInput(attrs={'placeholder': "Year of begin",
                                                         'size': '36',
                                                         'value': 'null'}), max_length=100)


class StudentGroup(forms.Form):
    group = forms.CharField(label="",
                           widget=forms.TextInput(attrs={'placeholder': "Group Number",
                                                         'size': '36',
                                                         'value': 'null'}), max_length=100)
