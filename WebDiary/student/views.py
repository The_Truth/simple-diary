from django.conf import settings

from django.shortcuts import render
from django.http import HttpResponseRedirect
from pymongo import MongoClient

from .forms import StudentFullnameForm
from .forms import StudentFioForm
from .forms import StudentFioRodForm
from .forms import StudentRankForm
from .forms import StudentIdDocumentConnect

from .forms import IdFormToShowStud

from .forms import DiscDocID
from .forms import StudentYear
from .forms import StudentGroup

# Create your views here.


def add_stud(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session add_stud: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        id_list = list()

        for post in posts.find():
            if 'description' not in list(post.keys()):
                id_list.append(post['_id'])

        if request.method == "POST":
            form_fullname = StudentFullnameForm(request.POST)
            form_fio = StudentFioForm(request.POST)
            form_fio_rod = StudentFioRodForm(request.POST)
            form_rank = StudentRankForm(request.POST)
            # form_id_doc = StudentIdDocumentConnect(request.POST)

            if form_fullname.is_valid() and form_fio.is_valid() and form_fio_rod.is_valid()\
                    and form_rank.is_valid():
                fullname = form_fullname.cleaned_data.get("fullname")
                fio = form_fio.cleaned_data.get("fio")
                fio_rod = form_fio_rod.cleaned_data.get("fio_rod")
                rank = form_rank.cleaned_data.get("rank")
                # id_doc = form_id_doc.cleaned_data.get("id_doc")

                id_doc = request.POST.get('Option_select')

                print(fullname, '\n', fio, '\n', fio_rod, '\n', rank, '\n', id_doc)

                post = posts.find_one(id_doc)
                # print(post)
                if post == None:
                    return render(request, 'student/add_stud.html', {'form_fullname': form_fullname,
                                                         'form_fio': form_fio,
                                                         'form_fio_rod': form_fio_rod,
                                                         'form_rank': form_rank,
                                                        # 'id_doc': form_id_doc,
                                                         'id_list': id_list,
                                                         'stud_msg_2': 'Не найден ни один документ дисциплины с таким ID. Попробуйте снова!'})
                else:
                    global_dict = dict()
                    task_dict = dict()
                    d = dict()
                    d_pz = dict()
                    pz_l = list()
                    d_lr = dict()
                    lr_l = list()
                    d_ddz = dict()
                    d_offs = dict()
                    d_exam = dict()
                    d_note = dict()

                    pz_num = 0
                    lr_num = 0
                    ddz_counter = False
                    offset_counter = False
                    exam_counter = False

                    extra_disc_counter = 0

                    # fullname = input('fullname студента = ')
                    # fullname = '2017-3-12-iva'
                    d['fullname'] = fullname
                    # fio = input('ФИО в им. падеже = ')
                    # fio = 'Иванов Иван Иванович'
                    d['fio'] = fio
                    # fio_rod = input('ФИО в род. падеже = ')
                    # fio_rod = 'Иванова Ивана Ивановича'
                    d['fio_rod'] = fio_rod
                    # rank = input('Звание = ')
                    # rank = 'рядовой'
                    d['rank'] = rank

                    global_dict['description'] = d

                    # Discipline_id = input('ID Дисциплины = ')
                    # Discipline_id = 1
                    Discipline_id = id_doc

                    full_disc_dict = posts.find_one({"_id": Discipline_id})

                    for x in full_disc_dict.keys():
                        extra_disc_counter += 1
                        if extra_disc_counter == 2:
                            Discipline_name = x

                    # Discipline_name = input('Дисциплина = ')
                    # Discipline_name = 'Subject_name_1'

                    # if full_disc_dict == None:
                        # print('no doc with that ID')
                        # return None

                    print(full_disc_dict)

                    print('pz_counter - ', full_disc_dict['pz_counter'])
                    print('lr_counter - ', full_disc_dict['lr_counter'])
                    print('ddz_counter - ', full_disc_dict['ddz_counter'])
                    print('offset_counter - ', full_disc_dict['offset_counter'])
                    print('exam_counter - ', full_disc_dict['exam_counter'])

                    student_id = fullname + '-' + str(Discipline_id)


                    # pz_num = int(input('Количество Практических Занятий = '))
                    # pz_num = 1
                    pz_num = full_disc_dict['pz_counter']
                    pz_n = full_disc_dict['pz_numbers']
                    pz_n.sort()

                    for i in range(1, pz_num + 1):
                        pz_l.append(d_pz)

                    name = 'PZ_'
                    marks_list = list()
                    disc_keys = list(full_disc_dict.keys())
                    subject = full_disc_dict[disc_keys[1]]
                    print(subject)
                    task_key = list(subject.keys())
                    print(task_key)
                    it = 0
                    for i in range(1, pz_num + 1):
                        print("echo")

                        # print(task_key)
                        task = subject[task_key[i-1]]
                        title_key = list(task.keys())
                        # print(title_key)

                        for title in title_key:
                            if 'title' in title and 'main_title' != title:
                                marks_list.append(" ")

                        tmp = marks_list.copy()
                        marks_list.clear()

                        pz_l[i - 1]['marks'] = tmp
                        pz_l[i - 1]['stud_date'] = ''
                        pz_l[i - 1]['plag'] = ''
                        pz_l[i - 1]['summary'] = ''

                        pz_l_tmp = pz_l[i - 1].copy()
                        pz_l[i - 1].clear()

                        print("pz_n: ", pz_n[i - 1])
                        while (it != pz_n[i - 1]):
                            it += 1

                        name += str(it)
                        task_dict[name] = pz_l_tmp
                        # print(pz_l)
                        name = 'PZ_'
                        global_dict[Discipline_name] = task_dict
                        print("GLOBAL DICT: ", global_dict)


                    print("\nGO TO LR\n")
                    # lr_num = int(input('Количество Лабораторных Работ = '))
                    # lr_num = 1
                    lr_num = full_disc_dict['lr_counter']
                    lr_n = full_disc_dict['lr_numbers']
                    lr_n.sort()

                    for i in range(1, lr_num + 1):
                        lr_l.append(d_lr)

                    name = 'LR_'
                    it = 0
                    q = 1
                    for ita in task_key:

                        if name in ita:

                            for i in range(1, lr_num + 1):
                                print("echo LR")

                                # print(task_key)
                                task = subject[task_key[q - 1]]
                                print(task)
                                title_key = list(task.keys())
                                print(title_key)
                                for title in title_key:
                                    print("ECHO LR 2")
                                    if 'title' in title and 'main_title' != title:
                                        print("ECHO LR 3")
                                        marks_list.append(" ")

                                tmp = marks_list.copy()
                                marks_list.clear()

                                lr_l[i - 1]['marks'] = tmp
                                lr_l[i - 1]['stud_date'] = ''
                                lr_l[i - 1]['plag'] = ''
                                lr_l[i - 1]['summary'] = ''

                                lr_l_tmp = lr_l[i - 1].copy()
                                lr_l[i - 1].clear()

                                while (it != lr_n[i - 1]):
                                    it += 1

                                name += str(it)
                                task_dict[name] = lr_l_tmp
                                name = 'LR_'
                                global_dict[Discipline_name] = task_dict

                        q += 1

                    q = 1

                    # answer = input("Будет ли ДДЗ? (да/нет) = ")
                    # answer = 'да'
                    ddz_counter = full_disc_dict['ddz_counter']

                    if ddz_counter == True:
                        # print("echo")

                        # print(task_key)
                        task = subject['DDZ']
                        title_key = list(task.keys())
                        for title in title_key:
                            if 'title' in title and 'main_title' != title:
                                marks_list.append(" ")

                        tmp = marks_list.copy()
                        marks_list.clear()

                        d_ddz['marks'] = tmp
                        d_ddz['stud_date'] = ''
                        d_ddz['plag'] = ''
                        d_ddz['summary'] = ''

                        d_ddz_tmp = d_ddz.copy()
                        d_ddz.clear()

                        task_dict['DDZ'] = d_ddz_tmp
                        global_dict[Discipline_name] = task_dict

                    offset_counter = full_disc_dict['offset_counter']

                    if offset_counter == True:
                        d_offs['offset_mark'] = ''

                        d_offs_tmp = d_offs.copy()
                        d_offs.clear()

                        task_dict['Offset'] = d_offs_tmp
                        global_dict[Discipline_name] = task_dict

                    # answer = input("Будет ли экзамен? (да/нет) = ")
                    # answer = 'да'
                    exam_counter = full_disc_dict['exam_counter']

                    if exam_counter == True:
                        d_exam['exam_mark'] = ''

                        d_exam_tmp = d_exam.copy()
                        d_exam.clear()

                        task_dict['Exam'] = d_exam_tmp
                        global_dict[Discipline_name] = task_dict

                    d_note['info'] = 'none'
                    d_note['link'] = 'none'

                    task_dict['note'] = d_note

                    # adding info about linked Discipline_document
                    task_dict['linked_disc_doc'] = Discipline_id

                    global_dict[Discipline_name] = task_dict

                    global_dict['_id'] = student_id

                    p = global_dict

                    if p != None:
                        posts.save(p)
                    else:
                        print("save_new_stud_doc - None")

                    print("Student document successfully created")
                    return render(request, 'student/add_stud.html', {'form_fullname': form_fullname,
                                                                     'form_fio': form_fio,
                                                                     'form_fio_rod': form_fio_rod,
                                                                     'form_rank': form_rank,
                                                                     # 'id_doc': form_id_doc
                                                                     'id_list': id_list,
                                                                     'stud_msg_1': 'Студент был успешно создан!'})
                # return HttpResponseRedirect('add_stud')

        # if a GET (or any other method) we'll create a blank form
        else:
            form_fullname = StudentFullnameForm()
            form_fio = StudentFioForm()
            form_fio_rod = StudentFioRodForm()
            form_rank = StudentRankForm()
            # form_id_doc = StudentIdDocumentConnect


        return render(request, 'student/add_stud.html', {'form_fullname': form_fullname,
                                                         'form_fio': form_fio,
                                                         'form_fio_rod': form_fio_rod,
                                                         'form_rank': form_rank,
                                                        # 'id_doc': form_id_doc
                                                          'id_list': id_list})
    else:
        return render(request, 'discipline/support.html')


def get_stud(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    global id

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session get_stud: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        id_list = list()


        disc_dict = dict()
        disc_dict_list = list()
        task_keys_list = list()

        data_list = list()
        global_data_list = list()


        keys_list = list()
        data_list = list()
        keys_and_data_dict = dict()
        keys_and_data_list = list()

        table_id = dict()
        j = 1

        # post = posts.find_one({'_id': 'timp_v1'})

        for post in posts.find():
            id_list.append(post["_id"])

        form_id = IdFormToShowStud(request.POST)

        if request.method == 'POST':
            if form_id.is_valid():
                id = form_id.cleaned_data.get("id_to_show")
                print(id)

                # return render(request, 'discipline/disc_table.html')
                return HttpResponseRedirect('get_stud/stud_table')
        else:
            print("NOT POST METHOD")
            form_id = IdFormToShowStud()

            post = {
                'id_list': id_list,
                'id_to_show': form_id,
            }

            return render(request, 'student/get_stud.html', post)
    else:
        return render(request, 'discipline/support.html')


def stud_table(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    global id

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session stud_table: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        disc_task_title_list = list()

        task_title_list = list()
        empty_task_title_list = list()
        task_title_dict = dict()
        all_task_title_list = list()

        # disc_dict = dict()
        # disc_dict_list = list()
        task_keys_list = list()

        # data_list = list()
        # global_data_list = list()

        # keys_list = list()
        # data_list = list()
        # keys_and_data_dict = dict()
        # keys_and_data_list = list()

        # table_id = dict()

        # server_dict = dict()
        # big_task_dict = dict()
        # task_dict = dict()

        # j = 1

        if request.method == 'GET':

            mongo_dict = posts.find_one(id)

            # print(mongo_dict)
            mongo_dict_keys = list(mongo_dict.keys())
            subject_dict = mongo_dict[mongo_dict_keys[2]]
            print(subject_dict)

            linked_disc_doc = subject_dict['linked_disc_doc']
            print(linked_disc_doc)

            # get title_names from linked Discipline and create list with all necessary titles (dict + stud)
            disc_dict = posts.find_one(linked_disc_doc)
            # print(disc_dict)
            disc_dict_keys = list(disc_dict.keys())
            all_task_dict = disc_dict[disc_dict_keys[1]]
            print(all_task_dict)
            all_task_dict_keys = list(all_task_dict.keys())

            for it in all_task_dict_keys:
                # print(it)
                # print(all_task_dict[it]['main_title'])
                task_dict = all_task_dict[it]
                task_dict_keys = list(task_dict.keys())
                for i in task_dict_keys:
                    if 'title' in i and i != 'main_title':
                        # print(task_dict[i])
                        disc_task_title_list.append(task_dict[i])

                subject_dict_keys = list(subject_dict.keys())

                for i in subject_dict_keys:
                    if it == i:
                        task_title_list += task_title_list + disc_task_title_list
                        for j in subject_dict[i]:
                            task_title_list.append(j)

                for i in range(0, len(task_title_list)):
                    empty_task_title_list.append(" ")

                # print(task_title_list)
                tmp_list = task_title_list.copy()
                empty_tmp_list = empty_task_title_list.copy()

                task_title_dict['titles'] = tmp_list
                task_title_dict['empty'] = empty_tmp_list
                tmp_dict = task_title_dict.copy()

                all_task_title_list.append(tmp_dict)

                task_title_list.clear()
                task_title_dict.clear()
                disc_task_title_list.clear()
                empty_task_title_list.clear()

            print("CHECK - ", all_task_title_list)

            # print(all_task_title_list)
            # print(len(all_task_title_list))



                # length = len(subject_dict_keys)

                # print("Subject_dict_keys - ", subject_dict_keys[j],'\nIt - ', it)
                # if subject_dict_keys[j] == it:
                    # print(it)
                    # j+=1
                # else:
                    # pass



    #############################################################################################################
            # disc_dict = posts.find_one(id)
            # print(id)

            # subject = list(disc_dict.keys())
            # print(subject[1])

            # subject_dict = disc_dict[subject[1]]
            # subject_keys = list(disc_dict[subject[1]].keys())

            #for it in subject_keys:
                # disc_dict_list.append(subject_dict[it])

                # task_items = subject_dict[it].items()
                # task_items_list = list(task_items)

                #print('\n\n', task_items)

                # keys_list.append('task')
                # data_list.append(it)
                # for d in task_items:
                    # keys_list.append(d[0])
                    # data_list.append(d[1])

                # tmp_1 = keys_list.copy()
                # tmp_2 = data_list.copy()
                # keys_list.clear()
                # data_list.clear()

                # keys_and_data_dict['keys'] = tmp_1
                # keys_and_data_dict['data'] = tmp_2
                # keys_and_data_dict['table_id'] = 'table_' + str(j)

                # tmp = keys_and_data_dict.copy()
                # keys_and_data_dict.clear()
                # keys_and_data_list.append(tmp)

                # j += 1

            # print(keys_and_data_list)
            # j = 0

            # print(keys_and_data_list)

            post = {
                 'id': id,
                 'title_list': all_task_title_list
             }
            print("GET")

            return render(request, 'student/stud_table.html', post)

        # if request.method == 'POST':
           # print("POST")
           # data = request.POST.get("data")
            # print(data)

           # mongo_dict = posts.find_one(id)
           # keys = list(mongo_dict.keys())

           # big_ajax_list = data.split('@')
           # for it in big_ajax_list:

               # ajax_list = it.split(';')
               # length = len(ajax_list) - 1
               # for i in range(1, int(length/2)+1):
                   # task_dict[ajax_list[i]] = ajax_list[int(length/2)+i]

               # tmp = task_dict.copy()
               # big_task_dict[ajax_list[0]] = tmp
               # task_dict.clear()

            # print(big_task_dict)
            # server_dict[keys[1]] = big_task_dict
            # print(server_dict)

            # server_dict['pz_counter'] = mongo_dict['pz_counter']
            # server_dict['lr_counter'] = mongo_dict['lr_counter']
            # server_dict['ddz_counter'] = mongo_dict['ddz_counter']
            # server_dict['offset_counter'] = mongo_dict['offset_counter']
            # server_dict['exam_counter'] = mongo_dict['exam_counter']
            # server_dict['pz_numbers'] = mongo_dict['pz_numbers']
            # server_dict['lr_numbers'] = mongo_dict['lr_numbers']

            # posts.update({"_id": id}, server_dict)

            # return render(request, 'student/stud_table.html', {'sth': data})

        print("ALL")
    else:
        return render(request, 'discipline/support.html')


def get_all_stud(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    global disc_id

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session get_all_stud: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        id_list = list()
        student_list = list()

        title_list = list()
        empty_title_list = list()

        year_list = list()
        group_list = [1, 2, 3, 4, 5]

        for i in range(2015, 2100):
            year_list.append(i)

        # form_doc_id = DiscDocID(request.POST)
        # form_year = StudentYear(request.POST)
        # form_group = StudentGroup(request.POST)

        for post in posts.find():
            if 'description' not in list(post.keys()):
                id_list.append(post["_id"])


        if request.method == 'POST':
            # if form_doc_id.is_valid() and form_year.is_valid() and form_group.is_valid():
                # disc_id = form_doc_id.cleaned_data.get("disc_id")
                # year = form_year.cleaned_data.get("year")
                # group = form_group.cleaned_data.get("group")
            disc_id = request.POST.get("Option_id")
            year = request.POST.get("Option_year")
            group = request.POST.get("Option_group")
            print(disc_id)
            print(year)
            print(group)


            post = posts.find_one(disc_id)
            # print(post)
            if post == None:
                return render(request, 'student/get_all_stud.html', {'id_list': id_list,
                                                                     'year_list': year_list,
                                                                     'group_list': group_list,
                                                                     'disc_msg': "Ни один документ дисциплины не найден с таким ID. "
                                                                                 "Попробуйте снова!"})
            else:
                title_list.append("fullname")

                post_keys = list(post.keys())
                # print(post)
                # print(post_keys)
                subject_name = post_keys[1]
                subject = post[post_keys[1]]
                subject_keys = list(subject.keys())
                print(subject_keys)

                # get titles from tasks
                for taskname in subject_keys:
                    title_list.append(taskname)

                    if 'PZ' in taskname.upper() or 'LR' in taskname.upper() or 'DDZ' in taskname.upper():

                        task = subject[taskname]
                        task_keys = list(task.keys())
                        # print(task_keys)

                        for key in task_keys:
                            if "title" in key and "main_title" != key:
                                # print(task[key])
                                title_list.append(task[key])

                                print("TITLE_LIST: ", title_list)

                        title_list.append("stud_date")
                        title_list.append("plag")
                        title_list.append("summary")
                    if 'OFFSET' in taskname.upper():
                        title_list.append("offset_mark")
                    if 'EXAM' in taskname.upper():
                        title_list.append("exam_mark")


                print("\ntitle_list: ", title_list)

                # create list of students in one Group and empty title list
                stud_counter = 0
                for post in posts.find():

                    print('post_id: ', post['_id'])
                    index = post['_id'].find('-')
                    tmp = post['_id']
                    # print(tmp[index + 1])

                    tmp_sub_id = post['_id'].split('-')
                    sub_list_len = len(tmp_sub_id)
                    # print('tmp_sub_id: ', tmp_sub_id, len(tmp_sub_id))

                    if disc_id in post["_id"] and year in post["_id"] and len(disc_id) == len(tmp_sub_id[sub_list_len-1]) \
                            and group in post["_id"] and int(year) in year_list and tmp[index+1] == str(group):

                        # print(post["_id"], ': ', post, '\n')
                        stud_counter += 1

                        stud_description = post["description"]

                        stud_keys = list(post.keys())
                        stud_perfomance_dict = post[stud_keys[2]]

                        empty_title_list.append(stud_description["fullname"])


                        for sub_key in subject_keys:
                            print('sub_key:', sub_key)
                            empty_title_list.append(" ")
                            print(stud_perfomance_dict)
                            inside_task = stud_perfomance_dict[sub_key]
                            if 'PZ' in sub_key.upper() or 'LR' in sub_key.upper() or 'DDZ' in sub_key.upper():
                                print('1')

                                print(inside_task)

                                for mark in inside_task['marks']:
                                    empty_title_list.append(mark)

                                empty_title_list.append(inside_task['stud_date'])
                                empty_title_list.append(inside_task['plag'])
                                empty_title_list.append(inside_task['summary'])
                            if 'OFFSET' in sub_key.upper():
                                print('2')
                                empty_title_list.append(inside_task['offset_mark'])
                            if 'EXAM' in sub_key.upper():
                                print('3')
                                print(inside_task)
                                empty_title_list.append(inside_task['exam_mark'])

                        tmp = empty_title_list.copy()

                        print(tmp)

                        student_list.append(tmp)
                        empty_title_list.clear()

                # print(student_list)
                if stud_counter == 0:
                    return render(request, 'student/get_all_stud.html', {# 'disc_id': form_doc_id,
                                                                            # 'year': form_year,
                                                                            # 'group': form_group,
                                                                             'id_list': id_list,
                                                                             'year_list': year_list,
                                                                             'group_list': group_list,
                                                                             'disc_msg': 'Студенты не найдены.'})
                else:
                    # return HttpResponseRedirect('get_all_stud/all_stud_table')
                    return render(request, 'student/all_stud_table.html', {'title_list': title_list,
                                                                           'student_list': student_list,
                                                                           'subject': subject_name})
        else:
            print("NOT POST METHOD")
            # form_doc_id = DiscDocID()
            # form_year = StudentYear()
            # form_group = StudentGroup()

            post = {
               # 'disc_id': form_doc_id,
               # 'year': form_year,
               # 'group': form_group,
                'id_list': id_list,
                'year_list':  year_list,
                'group_list': group_list
            }

            return render(request, 'student/get_all_stud.html', post)
    else:
        return render(request, 'discipline/support.html')


def all_stud_table(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session all_stud_table: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        title_list_number = list()
        marks = list()
        task = dict()

        tasknames = list()

        summary = 0

        task_server_dict = dict()
        server_dict = dict()

        if request.method == "POST":

            # found out how many titles include in one task - need for successfull parsing ajax data
            post = posts.find_one(disc_id)
            post_keys = list(post.keys())
            subject = post[post_keys[1]]
            subject_keys = list(subject.keys())

            for it in subject_keys:
                print(it)
                tasknames.append(it)
                task_keys = list(subject[it].keys())
                counter = 0
                for title in task_keys:
                    if 'title' in title and title != 'main_title':
                        counter += 1

                title_list_number.append(counter)

            print(title_list_number)

            # get data in right form from ajax-data
            data = request.POST.get("data")
            print('AJAX: ', data)

            data_list = data.split('@')
            # print(data_list)

            for it in data_list:
                tmp = it.split(';')
                print(tmp[0])

                j = 2
                while True:
                    for i in range(0, len(title_list_number)):

                        if 'PZ' in tasknames[i].upper() or 'LR' in tasknames[i].upper() or 'DDZ' in tasknames[i].upper():

                            oper_list = list()

                            mark_list = list()
                            final_mark_list = list()

                            symbol_flag = False

                            for n in range(0, title_list_number[i]):
                                # print("tmp: \n", tmp)
                                marks.append(tmp[j])
                                j += 1
                            m = marks.copy()
                            task['marks'] = m
                            marks.clear()

                            task['stud_date'] = tmp[j]
                            j += 1
                            task['plag'] = tmp[j]
                            j += 1

                            print("task", i, ": ", task)
                            formula_task = subject[subject_keys[i]]
                            formula_task_keys = list(formula_task.keys())

                            if 'formula' in formula_task_keys:

                                # print("formula: ", formula_task['formula'])
                                formula_list = formula_task['formula'].split()
                                # print(formula_list, '\n')

                                for it in formula_list:
                                    if 'mark' in it:
                                        mark_list.append(it)
                                    else:
                                        oper_list.append(it)

                                print("mark_list: ", mark_list, "\noper_list: ", oper_list)
                                # print("student_marks: ", task["marks"])
                                stud_marks = task['marks']

                                for it in mark_list:
                                    if '*' in it:
                                        point = it.split('*')
                                        # print(point)
                                        stud_point = point[1].split('_mark_')
                                        k = int(stud_point[1])
                                        # print("k: ", k)

                                        if '+' in stud_marks[k - 1]:
                                            temp = float(point[0]) * 1
                                            symbol_flag = True
                                        if '*' in stud_marks[k - 1]:
                                            temp = float(point[0]) * 0.5
                                            symbol_flag = True
                                        if '-' in stud_marks[k - 1]:
                                            temp = float(point[0]) * 0
                                            symbol_flag = True

                                        if symbol_flag == False:
                                            float_symbol = stud_marks[k - 1].replace(" ", "")
                                            if float_symbol == "":
                                                temp = 0
                                            else:
                                                # print("point[0]: ", point[0], "\nfloat_symbol: ", float_symbol)
                                                temp = float(point[0]) * float(float_symbol)

                                        symbol_flag = False

                                        final_mark_list.append(temp)

                                    else:
                                        stud_point = it.split('_mark_')
                                        k = int(stud_point[1])

                                        if '+' in stud_marks[k - 1]:
                                            temp = 1
                                            symbol_flag = True
                                        if '*' in stud_marks[k - 1]:
                                            temp = 0.5
                                            symbol_flag = True
                                        if '-' in stud_marks[k - 1]:
                                            temp = 0
                                            symbol_flag = True

                                        if symbol_flag == False:
                                            float_symbol = stud_marks[k - 1].replace(" ", "")
                                            if float_symbol == "":
                                                temp = 0
                                            else:
                                                temp = float(float_symbol)

                                        symbol_flag = False

                                print("mark_list_after_inside_operation: ", final_mark_list)

                                summary = 0
                                for t in range(0, len(oper_list)):
                                    if len(mark_list) != 0:
                                        if t == 0:
                                            summary = final_mark_list[0]

                                        if '+' in oper_list[t]:
                                            summary += final_mark_list[t+1]
                                        if '-' in oper_list[t]:
                                            summary -= final_mark_list[t+1]
                                        if '*' in oper_list[t]:
                                            summary *= final_mark_list[t+1]
                                        if '/' in oper_list[t]:
                                            summary /= final_mark_list[t+1]
                                    else:
                                        summary = 0
                                    # print(summary)

                                print(summary)

                            task['summary'] = summary
                            j += 1

                        if 'OFFSET' in tasknames[i].upper():
                            print('offset')
                            task['offset_mark'] = tmp[j]
                            j += 1
                            # print('tmp len: ', len(tmp), 'j: ', j)
                        if 'EXAM' in tasknames[i].upper():
                            task['exam_mark'] = tmp[j]
                            j += 1
                            print('exam')

                        server_task = task.copy()

                        print('server_task: ', server_task)

                        # print("j: ", j, '\ntmp_len: ', len(tmp))

                        task_server_dict[subject_keys[i]] = server_task

                        task.clear()
                        mark_list.clear()
                        final_mark_list.clear()
                        oper_list.clear()
                        j += 1

                    if i == len(title_list_number) - 1:
                        break

                # print(task_server_dict)

                server_tmp = task_server_dict.copy()
                server_dict[tmp[0]] = server_tmp
                task_server_dict.clear()

            # print(server_dict)
            server_fullname_dict_keys = list(server_dict.keys())
            # print(server_fullname_dict_keys)

            # paste changed data from html table to the MongoDB
            for it in server_fullname_dict_keys:
                # print(it)
                for post in posts.find():
                    if it in post["_id"] and disc_id in post["_id"]:
                        # print(post["_id"])
                        # print(server_dict[it])
                        mongo_keys = list(post.keys())
                        mongo_task = post[mongo_keys[2]]

                        server_task_keys = list(server_dict[it].keys())
                        # print(server_task_keys)
                        for server_key in server_task_keys:
                            mongo_task[server_key] = (server_dict[it])[server_key]
                            # print((server_dict[it])[server_key])

                        # print(mongo_task)
                        # print(post)
                        posts.update({"_id": post["_id"]}, post)

        return render(request, 'student/all_stud_table.html')
    else:
        return render(request, 'discipline/support.html')


def delete_stud(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session delete_stud: ", user, gr)

    if gr == 'admin' or gr == 'teacher':

        id_list = list()
        doc_id_list = list()
        year_list = list()

        for i in range(2015, 2100):
            year_list.append(i)

        for post in posts.find():
            if 'description' not in list(post.keys()):
                doc_id_list.append(post["_id"])

        # form_year = StudentYear()
        # form_id_doc = StudentIdDocumentConnect()

        if request.method == 'POST':
            print("POST")

            data = request.POST.get("data")
            print("AJAX student doc id: ", data)

            if type(data) is not type(None):

                index = data.find('-')
                tmp = data[index-4:len(data)]

                # print(tmp)
                posts.remove({"_id": tmp})

            # create a form instance and populate it with data from the request:
            # form_year = StudentYear(request.POST)
            # form_id_doc = StudentIdDocumentConnect(request.POST)

            stud_msg_1 = ""
            stud_msg_2 = ""

            # if form_year.is_valid() and form_id_doc.is_valid():
                # year = form_year.cleaned_data.get("year")
                # id_doc = form_id_doc.cleaned_data.get("id_doc")
            year = request.POST.get("Option_year")
            # print(year)
            id_doc = request.POST.get("Option_id")
            # print(id_doc)

            if year == 'Year List':
                stud_msg_2 = 'Введите год поступления!'
                return render(request, 'student/delete_stud.html', {'stud_msg_1': stud_msg_1,
                                                                    'stud_msg_2': stud_msg_2,
                                                                    'year_list': year_list,
                                                                    'doc_id_list': doc_id_list,
                                                                    'id_list': id_list})
            else:

                if id_doc in doc_id_list:
                    print(id_doc)

                    for post in posts.find():

                        tmp_sub_id = post['_id'].split('-')
                        sub_list_len = len(tmp_sub_id)

                        if year in post["_id"] and id_doc in post["_id"] and int(year) in year_list \
                                and len(id_doc) == len(tmp_sub_id[sub_list_len-1]):

                            id_list.append(post["_id"])
                        # print(id_list)
                    if len(id_list) == 0:
                        stud_msg_2 = "Ни один студент не был найден!"
                    else:
                        stud_msg_1 = "Студенты найдены!"

                else:
                    stud_msg_2 = "Ни один документ дисциплины с таким ID не был найден. Попробуйте снова!"

                return render(request, 'student/delete_stud.html', {'stud_msg_1': stud_msg_1,
                                                                    'stud_msg_2': stud_msg_2,
                                                                    'year_list': year_list,
                                                                    # 'form_year': form_year,
                                                                    # 'form_id_doc': form_id_doc,
                                                                    'doc_id_list': doc_id_list,
                                                                    'id_list': id_list})

        # if a GET (or any other method) we'll create a blank form
        else:
            print("GET")
            # form_year = StudentYear()
            # form_id_doc = StudentIdDocumentConnect()

            data = request.POST.get("data")
            print("AJAX student doc id: ", data)

            posts.remove({"_id": data})

        return render(request, 'student/delete_stud.html', {# 'form_year': form_year,
                                                            # 'form_id_doc': form_id_doc,
                                                            'doc_id_list': doc_id_list,
                                                            'year_list': year_list,
                                                            'stud_msg_1': "",
                                                            'stud_msg_2': ""})
    else:
        return render(request, 'discipline/support.html')


# for student to check group results (next 2 functions)
def student_all_stud_table(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    group_list = ['admin', 'teacher', 'student']

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session [student]all_stud_table: ", user, gr)

    if gr in group_list:

        id_list = list()

        title_list = list()
        empty_title_list = list()
        year_list = list()
        student_list = list()

        for i in range(2000, 2100):
            year_list.append(i)

        # profile = requests.get(
        #    url=settings.PROFILE_URL,
        #    cookies=request.COOKIES
        # ).json()
        ## profile = {
        ##     'created_at': '2018-09-13T08:16:44.431Z',
        ##     'name': '2017-3-08-kor',
        ##     'web_url': 'https://gitwork.ru/ivan_korotaev'
        ## }

        # buf = profile['name'].split('-')

        # only for debugging !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # fullname - ?
        year = 2017
        group = 3

        for post in posts.find():
            post_keys = list(post.keys())
            print(post_keys)

            if str(year) in post['_id'] and str(group) in post['_id'] and 'description' in post_keys:

                tmp = post['_id'].split('-')
                disc_id = tmp[len(tmp) - 1]
                print(disc_id)

                if disc_id not in id_list:
                    id_list.append(disc_id)


        if request.method == "POST":

            data = request.POST.get("Option_select")

            if data in id_list:

                post = posts.find_one(data)
                # print(post)
                if post == None:
                    disc_msg = "Ни один документ дисципины с таким ID не был найден. Попробуйте снова!"
                    return render(request, 'student/S_all_stud_table.html', {'id_list': id_list,
                                                                             'disc_msg': disc_msg})
                else:
                    title_list.append("fullname")

                    post_keys = list(post.keys())
                    # print(post)
                    # print(post_keys)
                    subject_name = post_keys[1]
                    subject = post[post_keys[1]]
                    subject_keys = list(subject.keys())
                    print(subject_keys)

                    # get titles from tasks
                    for taskname in subject_keys:
                        title_list.append(taskname)

                        task = subject[taskname]
                        task_keys = list(task.keys())
                        # print(task_keys)

                        for key in task_keys:
                            if "title" in key and "main_title" != key:
                                # print(task[key])
                                title_list.append(task[key])

                        title_list.append("stud_date")
                        title_list.append("plag")
                        title_list.append("summary")

                    print("\ntitle_list: ", title_list)

                    # create list of students in one Group and empty title list
                    stud_counter = 0
                    for post in posts.find():

                        print('post_id: ', post['_id'])
                        index = post['_id'].find('-')
                        tmp = post['_id']
                        print(tmp[index + 1])

                        if data in post["_id"] and str(year) in post["_id"] \
                                and str(group) in post["_id"] and int(year) in year_list and tmp[index+1] == str(group):

                            # print(post["_id"], ': ', post, '\n')
                            stud_counter += 1

                            stud_description = post["description"]

                            stud_keys = list(post.keys())
                            stud_perfomance_dict = post[stud_keys[2]]

                            empty_title_list.append(stud_description["fullname"])

                            for sub_key in subject_keys:
                                empty_title_list.append(" ")

                                inside_task = stud_perfomance_dict[sub_key]

                                for mark in inside_task['marks']:
                                    empty_title_list.append(mark)

                                empty_title_list.append(inside_task['stud_date'])
                                empty_title_list.append(inside_task['plag'])
                                empty_title_list.append(inside_task['summary'])

                            tmp = empty_title_list.copy()

                            # print(tmp)

                            student_list.append(tmp)
                            empty_title_list.clear()

                    # print(student_list)
                    if stud_counter == 0:
                        return render(request, 'student/S_all_stud_table.html', {'id_list': id_list,
                                                                             'disc_msg': 'Студенты не найдены.'})
                    else:
                        # return HttpResponseRedirect('get_all_stud/all_stud_table')
                        return render(request, 'student/S_all_stud_table_subject.html', {'title_list': title_list,
                                                                               'student_list': student_list,
                                                                               'subject': subject_name})

                # return render(request, 'student/S_all_stud_table_subject.html')
            else:
                disc_msg = "Ни один документ дисциплины с таким ID не был найден. Попробуйте снова!"
                return render(request, 'student/S_all_stud_table.html', {'id_list': id_list,
                                                                         'disc_msg': disc_msg})


        return render(request, 'student/S_all_stud_table.html', {'id_list': id_list})
    else:
        return render(request, 'discipline/support.html')


def student_all_stud_table_subject(request):
    client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT)
    db = client[settings.MONGO_DBNAME]
    posts = db.posts

    group_list = ['admin', 'teacher', 'student']

    user = request.session.get('username')
    gr = request.session.get('group')

    print("Current session [student]all_stud_table_subject: ", user, gr)

    if gr in group_list:
        return render(request, 'student/S_all_stud_table_subject.html')
    else:
        return render(request, 'discipline/support.html')