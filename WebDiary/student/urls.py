from django.urls import path
from . import views

urlpatterns = [
    path('student/add_stud', views.add_stud, name='add_stud'),
    path('student/get_stud', views.get_stud, name='get_stud'),
    path('student/get_stud/stud_table', views.stud_table, name='stud_table'),
    path('student/get_all_stud', views.get_all_stud, name='get_all_stud'),
    path('student/get_all_stud/all_stud_table', views.all_stud_table, name='all_stud_table'),
    path('student/delete_stud', views.delete_stud, name='delete_stud'),
    path('student/student_all_stud_table', views.student_all_stud_table, name='student_all_stud_table'),
    path('student/student_all_stud_table/student_all_stud_table_subject', views.student_all_stud_table_subject, name='student_all_stud_table subject')
]